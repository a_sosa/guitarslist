package com.figueroa.sosa.alex.guitarslist;
/**
 * Created by alex on 08-25-14.
 */
public class Guitar {

    public int id;
    public String make;
    public String model;
    public int year;
    public int error;

    public Guitar(String maketxt, String modeltxt, int yeartxt) {
        make = maketxt;
        model = modeltxt;
        year = yeartxt;
    }
    public Guitar(int idtxt){
        id = idtxt;
    }
    public Guitar(int idtxt, String maketxt, String modeltxt, int yeartxt) {
        id = idtxt;
        make = maketxt;
        model = modeltxt;
        year = yeartxt;
    }



}
