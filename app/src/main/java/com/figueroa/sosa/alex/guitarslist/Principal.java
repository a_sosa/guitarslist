package com.figueroa.sosa.alex.guitarslist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;


public class Principal extends Activity implements OnClickListener {

    Button btn_All, btn_brand, btn_Delete, btn_Create;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        initialize();

    }


    private void initialize(){
        btn_All = (Button) findViewById(R.id.btn_view_all);
        btn_All.setOnClickListener(this);
        btn_Delete = (Button) findViewById(R.id.btn_delete);
        btn_Delete.setOnClickListener(this);
        btn_Create = (Button) findViewById(R.id.btn_create);
        btn_Create.setOnClickListener(this);
    }


    public void onClick(View v){
        switch (v.getId()){
            case R.id.btn_view_all:
                startActivity(new Intent(this, View_guitar.class));
                break;
            case R.id.btn_create:
                startActivity(new Intent(this, New_guitar.class));
                break;
            case R.id.btn_delete:
                startActivity(new Intent(this, Destroy_guitar.class));
                break;
        }
    }

}
