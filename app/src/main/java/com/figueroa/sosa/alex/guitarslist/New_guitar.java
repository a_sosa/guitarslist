package com.figueroa.sosa.alex.guitarslist;

import android.app.Activity;
import android.content.Entity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class New_guitar extends Activity implements OnClickListener {
    private EditText make;
    private EditText model;
    private EditText year;
    private Button btnCrear, btnClear;
    private String maketxt, modeltxt, yeartxt;
    Gson gson = new Gson();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_guitar);
        make = (EditText)findViewById(R.id.edtmake);
        model = (EditText)findViewById(R.id.edtmodel);
        year = (EditText)findViewById(R.id.edtyear);
        btnCrear = (Button)findViewById(R.id.btn_Insert);
        btnCrear.setOnClickListener(this);
        btnClear = (Button)findViewById(R.id.btn_clean);
        btnClear.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        maketxt = make.getText().toString().toUpperCase();
        modeltxt = model.getText().toString().toUpperCase();
        yeartxt = year.getText().toString();
        switch (v.getId()){
            case R.id.btn_Insert:
                InsertGuitar insertGuitar = new InsertGuitar();
                if(maketxt.trim().isEmpty() | modeltxt.trim().isEmpty() | yeartxt.trim().isEmpty()){
                    Toast.makeText(getBaseContext(),"No se permiten campos vacios\nRevise la información que a ingresado ",Toast.LENGTH_SHORT).show();
                }else{
                    insertGuitar.execute(
                            maketxt,modeltxt,yeartxt
                    );
                }
                break;
            case R.id.btn_clean:
                make.setText("");
                model.setText("");
                year.setText("");
                break;

        }
    }

    private class InsertGuitar extends AsyncTask<String, Integer, Boolean> {
        private String respStr;
        int var;
        @Override
        protected Boolean doInBackground(String... params) {
            Boolean resul = true;
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://frozen-headland-1498.herokuapp.com/guitars/create");
            httpPost.setHeader("content-type", "application/json");
            try {
                //Generando Objeto Json donde se almacenaran los datos antes de ser enviados mediante el metodo post

                Guitar guitars = new Guitar(params[0], params[1], Integer.parseInt(params[2]));
                String json = gson.toJson(guitars);
                StringEntity stringEntity = new StringEntity(json);
                httpPost.setEntity(stringEntity);
                HttpResponse httpResponse = httpClient.execute(httpPost);
                respStr = EntityUtils.toString(httpResponse.getEntity());
                //Obteniendo respuesta del servidor {"error":1} si hubo error {"error":0 si no hubo error.
                Type type = new TypeToken<List<Guitar>>(){}.getType();
                ArrayList<Guitar> guitar = gson.fromJson(respStr, type);
                Guitar gui = guitar.get(0);
                var = gui.error;
                if (var == 1){
                    resul = false;
                }

            } catch (Exception e) {
                Log.e("ServicioRest", "Error!", e);
                resul = false;
            }


            return resul;
        }
        protected void onPostExecute(Boolean result) {
            if (result)
            {
                Toast.makeText(getBaseContext(), "Registro ingresado exitosamente ", Toast.LENGTH_SHORT).show();
                make.setText("");
                model.setText("");
                year.setText("");
            }
            else{
                Toast.makeText(getBaseContext(), "Ocurrio una error al hacer la petición al servidor\nes posible que no posea conexion a Internet\no es posible que ya exita dicho registro", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.new_guitar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
