package com.figueroa.sosa.alex.guitarslist;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class Destroy_guitar extends Activity {
    private Gson gson = new Gson();
    private GridView guitars;
    private AlertDialog.Builder builder;
    String[] val;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destroy_guitar);
        builder = new AlertDialog.Builder(this);
        guitars = (GridView)findViewById(R.id.gridView);
        ShowAll showAll = new ShowAll();
        showAll.execute();
        guitars.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                val = guitars.getItemAtPosition(position).toString().split(" ");
                builder.setMessage("¿Desea continuar con la eliminacion del registro?")
                        .setTitle("Advertencia") .setCancelable(false)
                        .setNegativeButton("Cancelar",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                })
                        .setPositiveButton("Continuar",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        DeleteGuitar deleteGuitar = new DeleteGuitar();
                                        deleteGuitar.execute(
                                                val[0]
                                        );
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

    }

    private class ShowAll extends AsyncTask<Void, Integer, Boolean> {
        private String respStr;
        private String[] guitarras;
        protected Boolean doInBackground(Void... params) {
            Boolean resul = true;
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://frozen-headland-1498.herokuapp.com/guitars/index");
            httpPost.setHeader("content-type", "application/json");
            try {
                Guitar guitars = new Guitar(0);
                String json = gson.toJson(guitars);
                StringEntity stringEntity = new StringEntity(json);
                httpPost.setEntity(stringEntity);
                HttpResponse response = httpClient.execute(httpPost);
                respStr = EntityUtils.toString(response.getEntity());
                //gson decode
                Type type = new TypeToken<List<Guitar>>(){}.getType();
                ArrayList<Guitar> guitar = gson.fromJson(respStr, type);
                guitarras = new String[guitar.size()];
                for( int i = 0; i < guitar.size(); i++){
                    Guitar gui= guitar.get(i);
                    guitarras[i] = gui.id +" " + gui.model;
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("ServicioRest", "Error!", e);
                resul = false;
            }

            return resul;
        }
        protected void onPostExecute(Boolean result) {
            if (result)
            {
                ArrayAdapter<String> adaptador =
                        new ArrayAdapter<String>(Destroy_guitar.this,
                                android.R.layout.simple_list_item_1,guitarras);
                guitars.setAdapter(adaptador);
            }
            else{
                Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private class DeleteGuitar extends AsyncTask<String, Integer, Boolean>{
        private String respStr;
        @Override
        protected Boolean doInBackground(String... params) {
            Boolean resul = true;
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://frozen-headland-1498.herokuapp.com/guitars/destroy");
            httpPost.setHeader("content-type", "application/json");
            try {
                Guitar guitars = new Guitar(Integer.parseInt(params[0]));
                String json = gson.toJson(guitars);
                StringEntity stringEntity = new StringEntity(json);
                httpPost.setEntity(stringEntity);
                HttpResponse httpResponse = httpClient.execute(httpPost);
                respStr = EntityUtils.toString(httpResponse.getEntity());
                //gson decode
                Type type = new TypeToken<List<Guitar>>(){}.getType();
                ArrayList<Guitar> guitar = gson.fromJson(respStr, type);
                Guitar gui = guitar.get(0);
                if( gui.error ==0){
                    resul=true;
                }
                else
                    resul=false;

            } catch (Exception e) {
                e.printStackTrace();
                Log.e("ServicioRest","Error!", e);
                resul = false;
            }

            return resul;
        }
        protected void onPostExecute(Boolean result) {
            if (result)
            {
                Toast.makeText(getBaseContext(), "Registro Eliminado exitosamente", Toast.LENGTH_SHORT).show();
                ShowAll showAll = new ShowAll();
                showAll.execute();
            }
            else{
                Toast.makeText(getBaseContext(), "Ocurrio una error al hacer la petición al servidor\nes posible que no posea conexion a Internet", Toast.LENGTH_SHORT).show();

            }
        }
    }
}
