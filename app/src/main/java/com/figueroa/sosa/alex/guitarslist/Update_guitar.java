package com.figueroa.sosa.alex.guitarslist;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class Update_guitar extends Activity implements View.OnClickListener {
    private EditText make;
    private EditText model;
    private EditText year;
    private Button btnUpdate, btnDelete;
    private String maketxt, modeltxt, yeartxt;
    private String id;
    Gson gson = new Gson();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_guitar);
        make = (EditText)findViewById(R.id.updmake);
        model = (EditText)findViewById(R.id.updmodel);
        year = (EditText)findViewById(R.id.updyear);
        btnUpdate = (Button)findViewById(R.id.btn_update);
        btnUpdate.setOnClickListener(this);
        FoundGuitar foundGuitar = new FoundGuitar();
        Bundle bundle = getIntent().getExtras();
        id =bundle.getString("ID");
        foundGuitar.execute(
                id
        );

    }


    @Override
    public void onClick(View v) {
        maketxt = make.getText().toString().toUpperCase();
        modeltxt = model.getText().toString().toUpperCase();
        yeartxt = year.getText().toString();
        switch (v.getId()){
            case R.id.btn_update:
                UpdateGuitar updateGuitar = new UpdateGuitar();
                if(id.trim().isEmpty() | maketxt.trim().isEmpty() | modeltxt.trim().isEmpty() | yeartxt.trim().isEmpty()){
                    Toast.makeText(getBaseContext(),"No se permiten campos vacios\nRevise la información que a ingresado ",Toast.LENGTH_SHORT).show();
                }else{
                    updateGuitar.execute(
                            id,maketxt,modeltxt,yeartxt
                    );
                }
                break;
            }
    }

    private class FoundGuitar extends AsyncTask<String, Integer, Boolean> {
        private String respStr, pstMake, pstModel;
        private int pstYear;
        @Override
        protected Boolean doInBackground(String... params) {
            Boolean resul = true;
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://frozen-headland-1498.herokuapp.com/guitars/show");
            httpPost.setHeader("content-type", "application/json");
            try {
                //Generando Objeto Json donde se almacenaran los datos antes de ser enviados mediante el metodo post
                Guitar guitars = new Guitar(Integer.parseInt(params[0]));
                String json = gson.toJson(guitars);
                StringEntity stringEntity = new StringEntity(json);
                httpPost.setEntity(stringEntity);
                HttpResponse httpResponse = httpClient.execute(httpPost);
                respStr = EntityUtils.toString(httpResponse.getEntity());
                //Obteniendo respuesta del servidor {"error":1} si hubo error {"error":0 si no hubo error.
                Type type = new TypeToken<List<Guitar>>(){}.getType();
                ArrayList<Guitar> guitar = gson.fromJson(respStr, type);
                Guitar gui = guitar.get(0);
                if( gui.id >=0){
                    resul=true;
                    pstYear = gui.year;
                    pstMake =gui.make;
                    pstModel =gui.model;
                }
                else
                    resul=false;

            } catch (Exception e) {
                Log.e("ServicioRest", "Error!", e);
                resul = false;
            }


            return resul;
        }
        protected void onPostExecute(Boolean result) {
            if (result)
            {
                make.setText(pstMake);
                model.setText(pstModel);
                year.setText(""+pstYear);
            }
            else{
                Toast.makeText(getBaseContext(), "Ocurrio una error al hacer la petición al servidor\nes posible que no posea conexion a Internet\no que el registro no exista", Toast.LENGTH_SHORT).show();
                make.setText("");
                model.setText("");
                year.setText("");
            }
        }
    }

    private class UpdateGuitar extends AsyncTask<String, Integer, Boolean> {
        private String respStr, pstMake, pstModel;
        private int pstYear;
        @Override
        protected Boolean doInBackground(String... params) {
            Boolean resul = true;
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://frozen-headland-1498.herokuapp.com/guitars/update");
            httpPost.setHeader("content-type", "application/json");
            try {
                //Generando Objeto Json donde se almacenaran los datos antes de ser enviados mediante el metodo post
                Guitar guitars = new Guitar(Integer.parseInt(params[0]), params[1],params[2],Integer.parseInt(params[3]));
                String json = gson.toJson(guitars);
                StringEntity stringEntity = new StringEntity(json);
                httpPost.setEntity(stringEntity);
                HttpResponse httpResponse = httpClient.execute(httpPost);
                respStr = EntityUtils.toString(httpResponse.getEntity());
                //Obteniendo respuesta del servidor {"error":1} si hubo error {"error":0 si no hubo error.
                Type type = new TypeToken<List<Guitar>>(){}.getType();
                ArrayList<Guitar> guitar = gson.fromJson(respStr, type);
                Guitar gui = guitar.get(0);
                if( gui.error ==0){
                    resul=true;
                }
                else
                    resul=false;
            } catch (Exception e) {
                Log.e("ServicioRest", "Error!", e);
                resul = false;
            }


            return resul;
        }
        protected void onPostExecute(Boolean result) {
            if (result)
            {
                Toast.makeText(getBaseContext(), "Registro Modificado exitosamente", Toast.LENGTH_SHORT).show();
                FoundGuitar foundGuitar = new FoundGuitar();
                foundGuitar.execute(
                        id
                );
            }
            else{
                Toast.makeText(getBaseContext(), "Ocurrio una error al hacer la petición al servidor\nes posible que no posea conexion a Internet", Toast.LENGTH_SHORT).show();

            }
        }
    }

}

